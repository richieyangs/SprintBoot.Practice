package com.thougthworks.repository;

import com.thougthworks.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by yrzhang on 10/10/16.
 */
public interface ShipwreckRepository extends JpaRepository<Shipwreck,Long> {

}
