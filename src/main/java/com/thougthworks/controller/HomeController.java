package com.thougthworks.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by yrzhang on 10/10/16.
 */

@RestController
public class HomeController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String home() {
        return "Hello, world";
    }
}
